<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Dashboard Rekam Medis</title>
	<link rel="icon" href="../images/itb.jpg"> <!-- icon -->
	<!-- Bootstrap Core CSS -->
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="../css/font-awesome.min.css" rel="stylesheet">
		
        <!-- Template js -->
        <script src="../js/jquery-2.1.1.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/jquery.appear.js"></script>

        <script src="../js/jqBootstrapValidation.js"></script>
        <script src="../js/modernizr.custom.js"></script>
        <script src="../js/script.js"></script>

<script src="../js/jquery.js"></script>	
</head>

<body>


       
 <header class="masthead bg-primary text-white text-center">
    <div class="container">
      <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
         <hr class="star-light">
      <h2 class="font-weight-light mb-0">Data  Medical record</h2>
    </div>
  </header>

<!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
		<table>
		<tr><td width= "40%"> Id Rekam Medis</td><td width= "10%">:</td><td width= "50%"><input id="idRekam" type="teks" disabled></td>
		<tr><td > Id Pasien</td><td >:</td><td ><input id="idpasien" type="teks" ></td>
		<tr><td > Berat Badan	</td><td >:</td><td ><input id="idberat" type="teks"></td>
		<tr><td > Tinggi Badan</td><td >:</td><td ><input id="idtinggi" type="teks"></td>
		<tr><td > Tensi</td><td >:</td><td ><input id="idtensi" type="teks"></td>
		<tr><td > diagnosa</td><td >:</td><td ><input id="iddiagnosa" type="teks"></td>
		<tr><td > tanggal</td><td >:</td><td ><input id="idtanggal" type="teks"></td>

		
		</tr>
		</table>
   	 
		 
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
		 		<span id="tombol">   
         		 </span>  
				   <!--<button type="button" class="btn btn-primary" onclick="simpan()" data-dismiss="modal">save</button>-->
				   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

        </div>
        
      </div>
    </div>
  </div>


<br>
<br>





<div class="col-md-2"></div>
<div class="col-md-8"> 
<button class="btn btn-primary pull-left" id="tampil">TAMPILKAN DATA </button>
<button class="btn btn-primary pull-left"  id="tampilJson" data-toggle="modal" data-target="#modaljson">DATA JSON</button> 
<button class="btn btn-info pull-right" id="tambah" onclick=tambah() data-toggle="modal" data-target="#myModal">TAMBAH DATA</button> 
<br></br>

<table class="table table-fill table-hover table-bordered " id="hasil" >    </table>
</div>   
<div class="col-md-2"></div>
       
      

  

 </body>

  <script type="text/javascript">



$("#tampil").click(function() {
reload();

});
    
function reload(){
	
  $.get( "http://127.0.0.1/api-health/api/medrec/?key=123",{}) 
    .done(function( result ) {

      console.log(result)
         $('#hasil').empty();
         $('#hasil').append('<tr><th >Id Rekam Medis</th><th>Id Pasien</th><th>Berat Badan</th><th>Tinggi Badan</th><th>Tensi</th><th>diagnosa</th><th>tanggal</th><th>edit</th><th>delete</th></tr>');
         $.each(result.data, function(i, item) {
		 $('#hasil').append('<tr><td>'+item.idmedrec+'</td><td>'+item.id_pasien+'</td><td>' +item.patientweight+'</td><td>' +item.patientheight+'</td><td>'
		 +item.patienttensi+'</td><td>'+item.patientdiagnose+'</td><td>'+item.datemedical+'</td><td><a href="#!" onclick=fEdit("'+item.idmedrec+'","'+item.id_pasien+'","'+item.patientweight+'","'+item.patientheight+'","'+item.patienttensi+'","'+item.patientdiagnose+'","'+item.datemedical+'") data-toggle="modal" data-target="#myModal">edit</a></td><td><a  href="#!" onclick=hapus("'+item.idmedrec+'")>delete</a></td></tr></tr>');
      });
  });	
	
}
	
	
function fEdit(id,idpasien, berat, tinggi,tensi, diagnosa,tanggal ){
$("#tombol").html('<button type="button" class="btn btn-primary" onclick="simpan(1)" data-dismiss="modal">UPDATE</button>');
$(".modal-title").text("EDIT DATA");
$("#idRekam").val(id);
$("#idpasien").val(idpasien);
$("#idberat").val(berat);
$("#idtinggi").val(tinggi);
$("#idtensi").val(tensi);
$("#iddiagnosa").val(diagnosa);
$("#idtanggal").val(tanggal);
}




function simpan(status){
	
	var id=$("#idRekam").val();
	var pasien=$("#idpasien").val();
	var berat=$("#idberat").val();
	var tinggi=$("#idtinggi").val();
	var tensi=$("#idtensi").val();
	var diagnosa=$("#iddiagnosa").val();
	var tanggal=$("#idtanggal").val();
	if (status==1)
	{
		$.ajax({
		url: 'http://127.0.0.1/api-health/api/medrec/'+id+'?key=123',
		data: {id_pasien: pasien, patientweight: berat, patientheight: tinggi, patienttensi: tensi, patientdiagnose: diagnosa,  datemedical: tanggal},
		type: 'PUT',
		success: function(result) {
		alert("update data berhsil");
		reload();
	   // Do something with the result
			}
		});
			
	}
	else if (status==2)
		{
			$.post("http://127.0.0.1/api-health/api/medrec/?key=123",{
			idmedrec:id, id_pasien: pasien, patientweight: berat, patientheight: tinggi, patienttensi: tensi, patientdiagnose: diagnosa,  datemedical: tanggal
			},function(result){alert("Penambahan data berhasil");
			reload();});

		}
}


function tambah(){
$("#tombol").html('<button type="button" class="btn btn-primary" onclick="simpan(2)" data-dismiss="modal">SIMPAN</button>');
$(".modal-title").text("TAMBAH DATA");
	$.get('http://127.0.0.1/api-health/api/medrec/?key=123', {}) .done(function(result)	{console.log(result)
	var n=result["data"].length;
	var idTerakhir=result["data"][n-1]["idmedrec"];
	var idBaru=Math.floor(idTerakhir.slice(2))+1;
	$("#idRekam").val("MR"+idBaru);	
	});
		
	
	var pasien=$("#idpasien").val("");
	var berat=$("#idberat").val("");
	var tinggi=$("#idtinggi").val("");
	var tensi=$("#idtensi").val("");
	var diagnosa=$("#iddiagnosa").val("");
	var tanggal=$("#idtanggal").val("<?php echo date('Y-m-d');?>");
	
	
}





function hapus(id){
	
	
	r = confirm("Yakin menghapus data dengan id = "+id+"?");
	if (r == true) {
				$.ajax({
				url: 'http://127.0.0.1/api-health/api/medrec/'+id+'?key=123',
				type: 'DELETE',
				success: function(result) {
					alert("penghapusan data dengan id = "+id+"  berhasil");
					reload();
				// Do something with the result
				}
				});
		
			} 

	
	
	
	
}



 $("#tampilJson").click(function() {
window.open(
  'http://127.0.0.1/api-health/api/medrec/?key=123',
  '_blank' // <- This is what makes it open in a new window.
);

}); 



</script>