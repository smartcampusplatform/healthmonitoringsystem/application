<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SMART HEALTH DASHBOARD</title>
		<link rel="icon" href="../images/itb.jpg"> <!-- icon -->

        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="../css/font-awesome.min.css" rel="stylesheet">
		
		<!-- Custom CSS -->
        <link href="../css/animate.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/style.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>


        <!-- Template js -->
        <script src="../js/jquery-2.1.1.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../js/jquery.appear.js"></script>
        <script src="../js/contact_me.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>
        <script src="../js/modernizr.custom.js"></script>
        <script src="../js/script.js"></script>

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    
    <body>
         <header class="masthead bg-primary text-white text-center">
    <div class="container">
      <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
         <hr class="star-light"> <div class="logo text-center">
     <h2>SMART HEALTH</h2>
                             <h1> INSTITUT TEKNOLOGI BANDUNG</h1> 
    </div> </div>
  </header>
        <!-- Start Logo Section -->
        <section id="logo-section" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                      
							</br>
							</br>
							</br>
							</br>
                       
                    </div>
                </div>
            </div>
        </section>
        <!-- End Logo Section -->
        
        
        <!-- Start Main Body Section -->
        <div class="mainbody-section text-center">
            <div class="container">
                <div class="row">
                    
					
					
					<div class="col-md-3">
                        
                       <div class="menu-item blue">
                            <a href="" data-toggle="modal">
                                <i class="fa fa-user"></i>
                                <p>PASIEN</p>
                            </a>
                        </div>
                        
                                <div class="menu-item pinky">
                                    <a href="" data-toggle="modal">
                                        <i class="fa fa-sort-numeric-asc"></i>
                                        <p>ANTRIAN</p>
                                    </a>
                                </div>
                          
                        
						
						
						
                        
                        
                    </div>
					
					
					
					<div class="col-md-6">
                        
						
                        
                        <div class="row">
                            <div class="col-md-3">
                                
                            </div>
                            
                            <div class="col-md-6">
                                <div class="menu-item color responsive">
                                    <a href="" data-toggle="modal">
                                        <i class="fa fa-medkit"></i>
                                        <p>MEDICINE</p>
                                    </a>
                                </div>
                            </div>
							<div class="col-md-3">
                                
                            </div>
                            
                        </div>
                      
						
                         <div class="row">
                            <div class="col-md-3">
                                
                            </div>
                            
                            <div class="col-md-6">
                                <div class="menu-item light-orange">
                                    <a href="dashboardMedrec.php" target="_blank" >
                                        <i class="fa fa-bar-chart"></i>
                                        <p>REKAM MEDIS</p>
                                    </a>
								</div>
                            </div>
							<div class="col-md-3">
                                
                            </div>
                            
                        </div>
						
						<div class="row">
                            <div class="col-md-3">
                                
                            </div>
                            
                            <div class="col-md-6">
                                <div class="menu-item red">
                                    <a href="" data-toggle="modal">
                                        <i class="fa fa-ambulance"></i>
                                        <p>EMERGENCY</p>
                                    </a>
								</div>
                            </div>
							<div class="col-md-3">
                                
                            </div>
                            
                        </div>
								
						
						
						
						
						
                        </div>
					
                   
                            
						
					
							
					
					
					<div class="col-md-3">
                        
                        <div class="menu-item light-green">
                            <a href="" data-toggle="modal">
                                <i class="fa fa-dollar"></i>
                                <p>PAYMENT</p>
                            </a>
                        </div>
                        
                       <div class="menu-item light-red">
									<a href="infoKesehatan.php" target="_blank" >
										<i class="fa fa-bullhorn"></i>
                                <p>INFO KESEHATAN</p>
                            </a>
                        </div>
                       
						
					 
                    </div>
					
                    
                    
					
					
					
					
					
                    
					
            </div>
        </div>
        <!-- End Main Body Section -->
        
        <!-- Start Copyright Section -->
        <div class="copyright text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div>@SMART_HEALTH LTI 2019</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Copyright Section -->
        
        
        <!-- Start Feature Section -->
                                
                       
                        
                    </div><!--/.row -->
                    
                </div>
                
            </div>
        </div>
        <!-- End Testimonial Section -->
        
    </body>
    
</html>